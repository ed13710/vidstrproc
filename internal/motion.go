package internal

import (
	"image"
	"image/color"

	"gocv.io/x/gocv"
)

var (
	mog2        = gocv.NewBackgroundSubtractorMOG2()
	minimumArea = float64(200)
	statusColor = color.RGBA{255, 0, 0, 0}
)

/*
MotionDetect
*/
func MotionDetect(img gocv.Mat, imgDelta gocv.Mat, imgThresh gocv.Mat) (bool, error) {

	output := false

	// first phase of cleaning up image, obtain foreground only
	mog2.Apply(img, &imgDelta)

	// remaining cleanup of the image to use for finding contours.
	// first use threshold
	gocv.Threshold(imgDelta, &imgThresh, 25, 255, gocv.ThresholdBinary)

	// then dilate
	kernel := gocv.GetStructuringElement(gocv.MorphRect, image.Pt(3, 3))
	defer kernel.Close()
	gocv.Dilate(imgThresh, &imgThresh, kernel)

	// now find contours
	contours := gocv.FindContours(imgThresh, gocv.RetrievalExternal, gocv.ChainApproxSimple)
	for i, c := range contours {
		area := gocv.ContourArea(c)
		if area < minimumArea {
			continue
		}

		output = true
		statusColor = color.RGBA{255, 0, 0, 0}
		gocv.DrawContours(&img, contours, i, statusColor, 2)

		rect := gocv.BoundingRect(c)
		gocv.Rectangle(&img, rect, color.RGBA{0, 0, 255, 0}, 2)
	}

	return output, nil
}
